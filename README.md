Installation instructions :
===========================

Run the following commands in a terminal :

```bash
apt-get install -y git nano

add-apt-repository -y ppa:it-samurai/squad

apt-get update
```

Proceed by setting up your [Homeless](https://bitbucket.org/maher-ops/homeless) specs, then launch a terminal :-D
