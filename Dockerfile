FROM    debian:stable

# Refresh the system
RUN ["apt-get", "update"]
RUN ["apt-get", "-qy", "upgrade"]
RUN ["apt-get", "clean"]
# Install packages
RUN ["apt-get", "install", "--force-yes", "-qy", "curl", "wget", "unzip", "build-essential", "cmake", "cython", "python2.7-dev", "ruby"]
