alias apt-update="apt-get update"
alias apt-search='aptitude search'
alias apt-install='apt-get install -y --force-yes'
alias apt-remove='apt-get remove -y --force-yes'
alias apt-clean='apt-get -y --force-yes autoremove && apt-get autoclean'
alias apt-upgrade='apt-get -y update && aptitude -y safe-upgrade'

